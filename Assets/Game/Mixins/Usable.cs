using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Usable : Mixin
{
    [SerializeField] MixinEvent useCB;

    void Use(Mixin m)
    {
        useCB.Invoke(m);
    }
}
