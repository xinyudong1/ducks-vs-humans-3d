using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class DuckLogic : MonoBehaviourPun, IDamageable
{
    [SerializeField] DuckInfoSO baseStats;
    [SerializeField] HealthBarController healthBar;
    NavMeshAgent agent;
    public Animator animator;

    [HideInInspector]
    public GameObject targetTower;
    private float health;

    [SerializeField] public ProjectileLogic projectile;
    [SerializeField] public GameObject projSpawnPosition;

    public Action<float, float> onHealthChanged;
    public Action<GameObject> onTargetChanged;
    public UnityEvent<DuckLogic> DuckDiedEvent = new UnityEvent<DuckLogic>();

    void Start()
    {
        if (baseStats == null)
            Debug.LogError($"Duck {gameObject.name} has no assigned Base Stats!");

        photonView.RPC("SetStartingHealthDuck", RpcTarget.All, baseStats.base_health);
        
        agent = GetComponent<NavMeshAgent>();

        agent.speed = baseStats.speed;
        onHealthChanged += healthBar.CalculateHealthBar;

        GameManager.Instance.AddDuck(this);

        // if networksettings isServer is false, disable all the components
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
        {
            Destroy(GetComponent<Melee_Duck_FSM_States>());
            Destroy(GetComponent<Medic_Duck_FSM_States>());
            Destroy(GetComponent<Scout_Duck_FSM_States>());
            Destroy(GetComponent<Sniper_Duck_FSM_States>());
            Destroy(agent);
        }

    }


    void Update()
    {
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;
        
        agent.speed = baseStats.speed;

        
        if (health <= 0.0f)
        {
            Die();
        }

    }


    //Delegate the setting target within DuckLogic because we want to send this to 2D manually for the 2D UI updating and targeting systems. 
    public void SetTarget(GameObject target)
    {

        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;

        targetTower = target;
        
        photonView.RPC("SendTargetTo2DRPC", RpcTarget.All, target.GetComponent<PhotonView>().ViewID);
    }

    [PunRPC]
    private void SendTargetTo2DRPC(int viewID)
    {
        //Want this handled only on 2D side
    }

    //Recieve the new Target from 2D UI RPC and set it within the DuckLogic
    [PunRPC]
    private void ConfirmedNewTargetRPC(int viewID)
    {

        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;
        Debug.Log("Recieved new orders from 2D side " + this.gameObject.name + " to target " + PhotonView.Find(viewID).gameObject.name);

        if (baseStats.type != DUCKTYPE.MEDIC)
        { 
            targetTower = PhotonView.Find(viewID).gameObject;
            onTargetChanged(targetTower);
        }

    }


    public DuckInfoSO GetBaseStats()
    {
        return baseStats;
    }

    public void TakeDamage(float _damage)
    {
        photonView.RPC("TakeDamageDuckRPC", RpcTarget.All, _damage);
    }
    
    [PunRPC]
    private void TakeDamageDuckRPC(float _damage)
    {
        health -= _damage;

        if (onHealthChanged != null)
            onHealthChanged(health, baseStats.base_health);

        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;


    }

    [PunRPC]
    private void SetStartingHealthDuck(float _health)
    {
        health = _health;
    }

    public void ApplyHeal(float _heal)
    {
        photonView.RPC("ApplyHealDuckRPC", RpcTarget.All, _heal);
    }

    [PunRPC]
    private void ApplyHealDuckRPC(float _heal)
    {
        health += _heal;

        if (health > baseStats.base_health)
        {
            health = baseStats.base_health;
        }
        if (onHealthChanged != null)
            onHealthChanged(health, baseStats.base_health);
    }

    private void Die()
    {
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;
        
        Debug.Log($"{gameObject.name} : I'm dead!");
        DuckDiedEvent.Invoke(this);
        GameManager.Instance.RemoveDuck(this);
        PhotonNetwork.Destroy(gameObject);
        //FSMController.Play("Die");
        //animator.Play("Die");
    }

    public float GetHealth()
    {
        return health;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, baseStats.attack_dist);


        if (Application.isPlaying && agent!=null)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(this.transform.position, agent.destination);
        }

    }
   

}
