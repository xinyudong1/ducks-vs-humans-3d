using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Default Duck", menuName = "ScriptableObjects/Duck", order = 1)]
public class DuckInfoSO : ScriptableObject
{
    public DUCKTYPE type;
    public float base_health;
    public float attack; // TO-DO replace with a Weapon Class
    public float speed;
    public float attack_dist;
    public float attack_cd;
}

public enum DUCKTYPE
{
    MELEE,
    MEDIC,
    SCOUT,
    RANGED
}
