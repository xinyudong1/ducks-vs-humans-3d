using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : Singleton<UIManager>
{
    public HealthBarController healthBar;
    public TMP_Text GoldCounterText;
    public TMP_Text TowerPlacerText;

    public void UpdateGold(uint gold)
    {
        if(GoldCounterText != null)
            GoldCounterText.text = $"Gold: {gold}";
    }

    public void UpdateTowerInfo(TowerPlacer.TowerInfo towerInfo)
    {
        if (TowerPlacerText != null)
        {
            TowerPlacerText.text = $"Tower: {towerInfo.type}\nCost:{towerInfo.cost}";
        }
    }

    //public void AddCallbacks()
    //{

    //    foreach (PlayerLogic player in GameManager.Instance.GetDefenders())
    //    {
    //        if (healthBar != null)
    //            player.AssignHealthBar(healthBar);
    //    }
    //}
}
