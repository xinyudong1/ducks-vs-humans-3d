using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckSpawnManager : Singleton<DuckSpawnManager>
{
    [SerializeField] GameSettingsSO settings;
    [SerializeField] Transform duckSpawnPoint;
    private int ducksSpawned = 0;

    float curr_time;

    private void Start()
    {
        curr_time = settings.duckSpawnTime;
        //StartCoroutine(SpawnDuckCoroutine());
    }



    IEnumerator SpawnDuckCoroutine()
    {

        while(ducksSpawned < settings.maxDuckCount)
        {
            Vector3 spawn = duckSpawnPoint.position;
            float x = Random.Range(spawn.x - settings.duckSpawnRadius, spawn.x + settings.duckSpawnRadius);
            float z = Random.Range(spawn.z - settings.duckSpawnRadius, spawn.z + settings.duckSpawnRadius);
            Transform duckTransform = new GameObject().transform;
            duckTransform.position = new Vector3(x, duckSpawnPoint.position.y, z);

            //Instantiate(settings.duck, duckTransform);
            ducksSpawned++;
            yield return new WaitForSeconds(settings.duckSpawnTime);
        }


    }

    public void SpawnDuck(DUCKTYPE _type, Vector3 _position)
    {
        switch(_type)
        {
            case DUCKTYPE.MELEE:
                PhotonNetwork.Instantiate(settings.meleeDuck.name, _position, Quaternion.identity);
                break;

            case DUCKTYPE.MEDIC:
                PhotonNetwork.Instantiate(settings.medicDuck.name, _position, Quaternion.identity);
                break;
            case DUCKTYPE.SCOUT:
                PhotonNetwork.Instantiate(settings.scoutDuck.name, _position, Quaternion.identity);
                break;
            case DUCKTYPE.RANGED:
                PhotonNetwork.Instantiate(settings.sniperDuck.name, _position, Quaternion.identity);
                break;
        }       
    }
}
