using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : Singleton<CameraManager>
{
    public CameraClassifier UISceneCamera;

    private Dictionary<string, CameraObject> CameraList = new Dictionary<string, CameraObject>();

    public T GetCameraObject<T>(CameraClassifier cameraClassifier) where T : CameraObject
    {
        CameraObject camera;
        if (CameraList.TryGetValue(cameraClassifier.name, out camera))
        {
            return (T)camera;
        }
        return null;
    }

    public void AddCameraObject(CameraObject camera)
    {
        if (CameraList.ContainsKey(camera.cameraClassifier.name))
        {
            Debug.Assert(false, $"CameraObject is already registered {camera.name}");
        }
        CameraList.Add(camera.cameraClassifier.name, camera);
    }

    public void RemoveCameraObject(CameraObject camera)
    {
        CameraList.Remove(camera.cameraClassifier.name);
    }

    public void EnableCamera(CameraClassifier cameraClassifier)
    {
        CameraObject camera;

        if (CameraList.TryGetValue(cameraClassifier.name, out camera))
        {
            camera.OnEnabled();
        }
    }

    public void DisableCamera(CameraClassifier cameraClassifier)
    {
        CameraObject camera;

        if (CameraList.TryGetValue(cameraClassifier.name, out camera))
        {
            camera.OnDisabled();
        }
    }
}
