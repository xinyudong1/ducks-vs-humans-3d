using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class TowerSpawnManager : Singleton<TowerSpawnManager>
{
    [SerializeField] GameSettingsSO settings;
    [SerializeField] PhotonView photonView;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }
    

    public void SpawnTower_RPC(TowerPlacer.TowerType _type, Vector3 _position)
    {
        photonView.RPC("SpawnTower", RpcTarget.All ,_type, _position);
    }

    [PunRPC]
    private void SpawnTower(TowerPlacer.TowerType _type, Vector3 _position)
    {
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;
        
        switch (_type)
        {
            case TowerPlacer.TowerType.Projectile:
                PhotonNetwork.Instantiate(settings.projectileTower.name, _position, Quaternion.identity);
                break;

            case TowerPlacer.TowerType.AOE:
                PhotonNetwork.Instantiate(settings.aoeTower.name, _position, Quaternion.identity);
                break;
            case TowerPlacer.TowerType.Mining:
                PhotonNetwork.Instantiate(settings.miningTower.name, _position, Quaternion.identity);
                break;
        }
    }
}
