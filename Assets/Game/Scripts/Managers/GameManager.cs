using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private List<Tower> towers = new List<Tower>();
    [SerializeField] private List<PlayerLogic> defenders = new List<PlayerLogic>();
    [SerializeField] private List<DuckLogic> ducks = new List<DuckLogic>();
    
    //Need to create a function to instantiate it on start so this can be set to private later on.
    [HideInInspector] public BaseController defenderBase;

    public Action<Tower> onTowerPlaced;
    public Action<Tower> onTowerDestroyed;

    public Action<DuckLogic> onDuckSpawned;
    public Action<DuckLogic> onDuckDestroyed;
    public Action<uint> onGoldChange;

    public Action<PlayerLogic> onDefenderSpawned;
    public Action<PlayerLogic> onDefenderDestroyed;

    [SerializeField] private GameSettingsSO settings;
    [SerializeField] private NetworkSettingsSO networkSettings;
    
    [SerializeField] private GameObject NetworkManager3DPrefab;

    [HideInInspector] public NetworkManager3D NetworkManager3D;

    [HideInInspector] public NetworkManager2D NetworkManager2D;

    [HideInInspector] public PhotonView photonView;
    void Start()
    {
        if (networkSettings.isServer == true)
        {
            PhotonNetwork.Instantiate(NetworkManager3DPrefab.name, Vector3.zero, Quaternion.identity, 0);
            PhotonNetwork.Instantiate(settings.defender.name, new Vector3(-21f, 1.4f, -17f), Quaternion.Euler(0, 40f, 0));

        }
        else
        {
            PhotonNetwork.Instantiate(settings.defender.name, new Vector3(-15f, 1.4f, -21f), Quaternion.Euler(0, 40f, 0));
        }
        MenuManager.Instance.ShowMenu(MenuManager.Instance.HudMenu);
        StartCoroutine(PassiveResourceAdd());

    }
    void Update()
    {
        
    }

    #region TOWERS
    public List<Tower> GetTowers()
    {
        return towers;
    }

    public void AddTower(Tower _tower)
    {
        towers.Add(_tower);

        if(onTowerPlaced != null)
        {
            onTowerPlaced(_tower);
        }
    }

    public void RemoveTower(Tower _tower)
    {
        towers.Remove(_tower);
        if(onTowerDestroyed != null)
        {
            onTowerDestroyed(_tower);
        }
    }

    #endregion

    #region DUCKS
    public List<DuckLogic> GetDucks()
    {
        return ducks;
    }

    public void AddDuck(DuckLogic duck)
    {
        ducks.Add(duck);

        if(onDuckSpawned != null)
        {
            onDuckSpawned(duck);
        }
    }

    public void RemoveDuck(DuckLogic duck)
    {
        ducks.Remove(duck);
        if (onDuckDestroyed != null)
        {
            onDuckDestroyed(duck);
        }
    }
    #endregion

    #region DEFENDER/PLAYER
    public List<PlayerLogic> GetDefenders()
    {
        return defenders;
    }

    public void AddDefender(PlayerLogic _defender)
    {
        defenders.Add(_defender);

        if (onDefenderSpawned != null)
        {
            onDefenderSpawned(_defender);
        }
    }

    public void RemoveDefender(PlayerLogic _defender)
    {
        defenders.Remove(_defender);

        if (onDefenderDestroyed != null)
        {
            onDefenderDestroyed(_defender);
        }
    }
    #endregion

    IEnumerator PassiveResourceAdd()
    {
        while (true)
        {
            AddGold(settings.passiveAmount);
            yield return new WaitForSeconds(settings.passiveTime);
        }
    }

    public void AddGold(uint _gold)
    {
        foreach (PlayerLogic player in defenders)
        {
            player.IncreaseGold(_gold);
            if (onGoldChange != null)
                onGoldChange(player.GetGold());
        }
    }

    public void RemoveGold(uint _gold)
    {
        foreach (PlayerLogic player in defenders)
        {
            player.DecreaseGold(_gold);
            if (onGoldChange != null)
                onGoldChange(player.GetGold());
        }
    }

    public NetworkSettingsSO GetNetworkSettings()
    {
        return networkSettings;
    }
}
