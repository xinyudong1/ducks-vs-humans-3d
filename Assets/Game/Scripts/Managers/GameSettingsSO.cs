using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "ScriptableObjects/GameSettings")]
public class GameSettingsSO : ScriptableObject
{
    public float passiveTime;
    public uint passiveAmount;

    public float duckSpawnTime;
    public float duckSpawnRadius;
    public int maxDuckCount;
    public GameObject meleeDuck;
    public GameObject medicDuck;
    public GameObject scoutDuck;
    public GameObject sniperDuck;
    public GameObject defender;
    public GameObject projectileTower;
    public GameObject miningTower;
    public GameObject aoeTower;
}
