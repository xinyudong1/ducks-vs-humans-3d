using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerLogic))]
//Temporary movement for testing purposes
public class PlayerInputController : MonoBehaviourPun
{
    [SerializeField] private PlayerLogic playerLogic;
    private PlayerGamepadController inputActions;

    [SerializeField] private float mouseSens;
    [SerializeField] private float moveSens;

    // Start is called before the first frame update
    void Awake()
    {
        if (photonView.IsMine == false)
            return;
        
        if (playerLogic == null)
            playerLogic = GetComponent<PlayerLogic>();

        inputActions = new PlayerGamepadController();

        

        inputActions.Controller.Look.started += Look_started;
        inputActions.Controller.Attack.started += playerLogic.Attack_started;
        inputActions.Controller.SpawnTower.started += playerLogic.TowerPlacementMode;
        inputActions.Controller.Melee.started += playerLogic.MeleeAttackMode;
        inputActions.Controller.SwitchTower.started += playerLogic.CycleTower;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Look_started(InputAction.CallbackContext obj)
    {
        if (photonView.IsMine == false)
            return;
        
        Vector2 look = obj.ReadValue<Vector2>() * mouseSens * Time.deltaTime;
        playerLogic.RotateCamera(look.x, look.y); //TODO Figure out how to properly clamp rotation
    }

    private void OnEnable()
    {
        if (photonView.IsMine == false)
            return;
        inputActions.Enable();
    }
    private void OnDisable()
    {
        if (photonView.IsMine == false)
            return;
        inputActions.Disable();
    }

    private void FixedUpdate()
    {
        if (photonView.IsMine == false)
            return;
        
        #region new input map
        Vector2 move = inputActions.Controller.Move.ReadValue<Vector2>() * moveSens * Time.deltaTime;
        playerLogic.Move(new Vector3(move.x, 0, move.y));

        #endregion
    }


}
