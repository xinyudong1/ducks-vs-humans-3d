using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerLogic : MonoBehaviourPun, IDamageable
{
    public enum PlayerAttackMode
    {
        Melee,
        TowerPlacement
    }
    
    private float Health;
    private float MaxHealth;
    private float Damage;
    private float MoveSpeed;
    private float MouseSpeedX;
    private float MouseSpeedY;
    private float AttackDist;
    private float AttackCD;
    private uint gold;

    private float attackTime = 0.0f;
    private float rotX = 0.0f;


    [SerializeField] private Camera fpsCamera;
    [SerializeField] private GameObject head; //For camera rotation
    [SerializeField] private HealthBarController healthBar;
    [SerializeField] private PlayerInfo baseStats;
    private PlayerAttackMode attackMode = PlayerAttackMode.Melee;

    [Header("Tower Placement")]
    
    [SerializeField] private TowerPlacer.TowerType currentTower = TowerPlacer.TowerType.Projectile;
    private List<TowerPlacer.TowerType> towerTypes = Enum.GetValues(typeof(TowerPlacer.TowerType)).Cast<TowerPlacer.TowerType>().ToList();

    [SerializeField] private Transform placementTransform;
    [SerializeField] private GameObject towerPlacerPrefab;

    [SerializeField] private TowerPlacer towerPlacer = null;
    
    public Action<float, float> onHealthChange;
    public Action<TowerPlacer.TowerInfo> onTowerChange;

    // Start is called before the first frame update
    void Start()
    {
        if (baseStats == null)
            Debug.LogError($"Player {gameObject.name} has no assigned Base Stats!");


        if (fpsCamera == null)
            Debug.LogError($"Player {gameObject.name}: fpsCamera is not set!");

        //inputActions = new PlayerGamepadController();

        Health = baseStats.BaseHealth;
        MaxHealth = Health;
        Damage = baseStats.BaseDamage;
        MoveSpeed = baseStats.MoveSpeed;
        // TODO Move MouseSpeed X and Y to settings SO
        MouseSpeedX = baseStats.MouseSpeedX;
        MouseSpeedY = baseStats.MouseSpeedY;
        AttackDist = baseStats.AttackDist;
        AttackCD = baseStats.AttackCD;

        GameManager.Instance.AddDefender(this);

        if(photonView.IsMine == false)
        {
            Destroy(GetComponentInChildren<Camera>());
            Destroy(GetComponentInChildren<AudioListener>());
            fpsCamera = null;
        }
        else
        {
            onHealthChange += UIManager.Instance.healthBar.CalculateHealthBar;
            onTowerChange += UIManager.Instance.UpdateTowerInfo;
            //UIManager.Instance.UpdateTowerInfo(towerPlacer.GetTowerInfo(currentTower));
            GameManager.Instance.onGoldChange += UIManager.Instance.UpdateGold;
        }    
    }

    public void AssignHealthBar(HealthBarController _healthBar)
    {
        if(photonView.IsMine)
            healthBar = _healthBar;
    }

    public void Attack_started(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (photonView.IsMine == false)
            return;
        
        if (attackTime <= 0.0f)
        {
            attackTime = AttackCD;
            Attack();
        }
    }
    
    public void TowerPlacementMode(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (attackMode == PlayerAttackMode.TowerPlacement)
            return;

        attackMode = PlayerAttackMode.TowerPlacement;

        if (UIManager.Instance.TowerPlacerText != null)
        {
            UIManager.Instance.TowerPlacerText.enabled = true;
            
        }

        if (towerPlacerPrefab != null)
        {
            GameObject towerPlacerObj = Instantiate(towerPlacerPrefab, transform.position, Quaternion.identity);
            towerPlacer = towerPlacerObj.GetComponent<TowerPlacer>();
            UIManager.Instance.UpdateTowerInfo(towerPlacer.GetTowerInfo(currentTower));
            towerPlacer.SetPlayer(this);
            
            if (placementTransform != null)
                towerPlacer.followTransform = placementTransform;

        }
    }

    public void MeleeAttackMode(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (attackMode == PlayerAttackMode.Melee)
            return;
        
        attackMode = PlayerAttackMode.Melee;

        if (UIManager.Instance.TowerPlacerText != null)
            UIManager.Instance.TowerPlacerText.enabled = false;

        if (towerPlacer != null)
        {
            Destroy(towerPlacer.gameObject);
            towerPlacer = null;
        }
    }

    public void CycleTower(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (attackMode != PlayerAttackMode.TowerPlacement)
            return;
        
        int i = towerTypes.IndexOf(currentTower);
        
        if (i == towerTypes.Count - 1)
        {
            currentTower = towerTypes[0];
        }
        else
        {
            currentTower = towerTypes[i + 1];
        }
        onTowerChange(towerPlacer.GetTowerInfo(currentTower));
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine == false)
            return;
        
        if (attackTime > 0.0f)
        {
            attackTime -= Time.deltaTime;
        }
    }

    public void Move(Vector3 direction)
    {
        transform.Translate(direction * (MoveSpeed * Time.deltaTime));
    }

    public void RotateCamera(float vertical, float horizontal)
    {
        if (head != null)
        {
            rotX -= MouseSpeedX * horizontal;

            rotX = Mathf.Clamp(rotX, -90f, 40f);

            head.transform.localEulerAngles = new Vector3(rotX, 0, 0); // TODO Find a better way to implement camera rotation
            transform.Rotate(0, vertical * MouseSpeedY, 0);
        }
    }

    private void Attack()
    {
        switch(attackMode)
        {
            case PlayerAttackMode.Melee:
                MeleeAttack();
                break;
            case PlayerAttackMode.TowerPlacement:
                PlaceTower();
                break;
            default:
                break;
        }
    }

    public void PlaceTower()
    {
        if (towerPlacer != null)
        {
            towerPlacer.TowerPlacedListener += FinishTowerPlacement;
            towerPlacer.PlaceTower(currentTower);
        }
    }

    private void FinishTowerPlacement()
    {
        if (towerPlacer == null)
            return;

        Destroy(towerPlacer.gameObject);
        towerPlacer = null;

        attackMode = PlayerAttackMode.Melee;

        if (UIManager.Instance.TowerPlacerText != null)
            UIManager.Instance.TowerPlacerText.enabled = false;
    }

    private void MeleeAttack()
    {
        Ray ray = fpsCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        ray.origin = fpsCamera.transform.position;

        if (Physics.Raycast(ray, out RaycastHit hit, AttackDist))
        {
            DuckLogic duck = hit.collider.gameObject.GetComponent<DuckLogic>();

            if (duck != null)
            {
                duck.TakeDamage(Damage);
            }
        }
    }

    public void TakeDamage(float damage)
    {
        Health -= damage;

        if (onHealthChange != null)
            onHealthChange(Health, MaxHealth);

        if(Health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Debug.Log($"{gameObject.name} : I'm dead!");
        GameManager.Instance.RemoveDefender(this);
        Destroy(gameObject);
    }

    public void IncreaseGold(uint _gold)
    {
        gold += _gold;
    }
    
    public void DecreaseGold(uint _gold)
    {
        gold -= _gold;
    }

    public uint GetGold()
    {
        return gold;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * AttackDist);

    }
}
