using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerStats", menuName = "ScriptableObjects/PlayerStats", order = 1)]
public class PlayerInfo : ScriptableObject
{
    public float BaseHealth;
    //TODO weapon/item slots
    public float MoveSpeed;
    public float MouseSpeedX;
    public float MouseSpeedY;
    public float BaseDamage;
    public float AttackDist;
    public float AttackCD;

    public string NickName = "Player";

}