using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LavaTower : Tower
{
    // Stats
    [SerializeField] TowerInfoSO towerInfo;
    public float Damage { get; private set; } = 10f;
    public float AttackDist { get; private set; } = 10f;
    public float AttackCD { get; private set; } = 1f;

    //Targeting
    public List<GameObject> targets = new List<GameObject>();

    // Calculations/Timesrs
    private float attackTime = 0.0f;

    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();

        LoadInfo();

        if (GameManager.Instance.GetNetworkSettings().isServer == false)
        {
            //Destroy the FSM since we're not server
            Destroy(GetComponent<FloorisLavaTowerFSM>());
            Destroy(GetComponent<NavMeshObstacle>());
        }

        photonView.RPC("SetStartingHealthLavaTower", RpcTarget.All, MaxHealth);
    }

    private void LoadInfo()
    {
        if (towerInfo != null)
        {
            Health = towerInfo.BaseHealth;
            MaxHealth = Health;
            Damage = towerInfo.BaseDamage;
            AttackDist = towerInfo.AttackDist;
            AttackCD = towerInfo.AttackCD;
        }
        else
        {
            Debug.LogWarning($"{gameObject.name} : Missing TowerInfo Scriptable Object");
        }
    }

    public void Attack()
    {
        foreach (var target in targets)
        {
            if (target.GetComponent<DuckLogic>() != null)
            {
                target.GetComponent<DuckLogic>().TakeDamage(Damage);
                Debug.Log(target.name + " took " + Damage + " damage");
            }
        }                
    }

    private void OnDrawGizmos()
    {
        
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, AttackDist);
        
        foreach (var target in targets)
        {
            if (target != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, target.transform.position);
            }
        }
    }

    public override void TakeDamage(float _damage)
    {
        photonView.RPC("TakeDamageTowerLavaRPC", RpcTarget.All, _damage);
    }

    // rpc function to sync health of the tower
    [PunRPC]
    private void TakeDamageTowerLavaRPC(float _damage)
    {
        Health -= _damage;

        if (onHealthChanged != null)
            onHealthChanged(Health, MaxHealth);

        if (Health <= 0)
        {
            Die();
        }
    }

    [PunRPC]
    private void SetStartingHealthLavaTower(float _health)
    {
        Health = _health;
    }

}
