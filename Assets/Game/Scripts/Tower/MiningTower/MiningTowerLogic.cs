using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MiningTowerLogic : Tower
{
    [SerializeField] MiningTowerSO towerInfo;
    public uint Income { get; private set; } = 2;
    public float IncomeCD { get; private set; } = 5f;

    private void LoadInfo()
    {
        if (towerInfo != null)
        {
            Health = towerInfo.BaseHealth;
            MaxHealth = Health;
            Income = towerInfo.Income;
            IncomeCD = towerInfo.IncomeCD;
        }
        else
        {
            Debug.LogWarning($"{gameObject.name} : Missing TowerInfo Scriptable Object");
        }

    }

    IEnumerator AddGold()
    {
        while (true)
        {
            GameManager.Instance.AddGold(Income);
            yield return new WaitForSeconds(IncomeCD);
        }
    }

    protected override void Start()
    {
        base.Start();
        
        LoadInfo();

        if (GameManager.Instance.GetNetworkSettings().isServer == false)
        {
            Destroy(GetComponent<TowerFSM>());
            Destroy(GetComponent<NavMeshObstacle>());
            return;
        }
        photonView.RPC("SetStartingHealthTower", RpcTarget.All, MaxHealth);
        StartCoroutine(AddGold());
    }

    public override void TakeDamage(float _damage)
    {
        photonView.RPC("TakeDamageTowerRPC", RpcTarget.All, _damage);
    }

    // rpc function to sync health of the tower
    [PunRPC]
    private void TakeDamageTowerRPC(float _damage)
    {
        Health -= _damage;

        if (onHealthChanged != null)
            onHealthChanged(Health, MaxHealth);

        if (Health <= 0)
        {
            Die();
        }
    }

    [PunRPC]
    private void SetStartingHealthTower(float _health)
    {
        Health = _health;
    }
}
