using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MiningTowerStats", menuName = "ScriptableObjects/Tower Stats/Mining Tower Stats", order = 2)]
public class MiningTowerSO : ScriptableObject
{
    public float BaseHealth;
    public uint Income;
    public float IncomeCD;
}
