using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tower : MonoBehaviourPun, IDamageable
{
    // Don't hardcode this. Much better for this in a variable for networking on the 2D side!!!!
    public float Health { get; protected set; } = 100f;
    public float MaxHealth { get; protected set; } = 100f;

    [SerializeField] protected HealthBarController healthBar;

    public Action<float, float> onHealthChanged;
    public Action onDeath;

    protected virtual void Start()
    {
        GameManager.Instance.AddTower(this);

        onHealthChanged += healthBar.CalculateHealthBar;
        onDeath += OnDeath;

        
    }

    public virtual void TakeDamage(float _damage)
    {
        // moving this to derived classes for PUN reasons
    }


    protected void Die()
    {
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;
        onDeath();
        Debug.Log($"{gameObject.name} : I'm dead!");
        GameManager.Instance.RemoveTower(this);
        PhotonNetwork.Destroy(gameObject);
    }

    protected virtual void OnDeath()
    {
    }
}
