using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Default Base", menuName = "ScriptableObjects/Defender Base")]

public class BaseInfoSO : ScriptableObject
{
    [SerializeField] private float base_health;
    public GameObject prefab;
    public Transform spawnPosition;

    public float GetBaseHealth()
    {
        return base_health;
    }


}
