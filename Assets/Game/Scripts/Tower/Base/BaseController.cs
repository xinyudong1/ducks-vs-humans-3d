using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviourPun, IDamageable
{
    [SerializeField] HealthBarController healthBar;
    [SerializeField] BaseInfoSO baseInfo;
    float health;
    public Action<float, float> onHealthChanged;

    private void Start()
    {
        GameManager.Instance.defenderBase = this;

        onHealthChanged += healthBar.CalculateHealthBar;

        // if not server return
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
        {
            return;
        }

        photonView.RPC("SetStartingHealthBase", RpcTarget.All, baseInfo.GetBaseHealth());
    }
    public void TakeDamage(float _damage)
    {
        photonView.RPC("TakeDamageBaseRPC", RpcTarget.All, _damage);
    }

    void DefenderLose()
    {
        if (GameManager.Instance.GetNetworkSettings().isServer == true)
        {
            PhotonNetwork.Destroy(gameObject);        
        }

        GameManager.Instance.NetworkManager3D.Lose();
    }

    [PunRPC]
    private void TakeDamageBaseRPC(float _damage)
    {
        health -= _damage;

        if (onHealthChanged != null)
            onHealthChanged(health, baseInfo.GetBaseHealth());

        if (health <= 0.0f)
            DefenderLose();
    }

    [PunRPC]
    private void SetStartingHealthBase(float _health)
    {
        health = _health;
    }

    
}
