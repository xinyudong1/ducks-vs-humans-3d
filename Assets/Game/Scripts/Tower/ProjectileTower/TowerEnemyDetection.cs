using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class TowerEnemyDetection : MonoBehaviour
{
    [SerializeField] private SphereCollider trigger;
    [SerializeField] private TowerLogic tower;

    private void Start()
    {
        if (tower == null)
        {
            tower = GetComponentInParent<TowerLogic>();

            if (tower == null)
            {
                Debug.LogWarning($"{gameObject.name} : Missing TowerLogic component!");
                Destroy(this);
            }
        }

        if (trigger == null)
            trigger = GetComponent<SphereCollider>();

        trigger.radius = tower.AttackDist;
    }

    private void OnTriggerEnter(Collider other)
    {
        DuckLogic duck = other.GetComponent<DuckLogic>();

        if (duck != null)
        {
            tower.targets.Add(other.gameObject);
            duck.DuckDiedEvent.AddListener(OnDuckDead);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        DuckLogic duck = other.GetComponent<DuckLogic>();

        if (duck != null)
        {
            tower.targets.Remove(other.gameObject);
            duck.DuckDiedEvent.RemoveListener(OnDuckDead);
        }
    }

    private void OnDuckDead(DuckLogic duck)
    {
        tower.targets.Remove(duck.gameObject);
        duck.DuckDiedEvent.RemoveListener(OnDuckDead);
    }
}
