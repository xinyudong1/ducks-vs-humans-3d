using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TowerLogic : Tower
{
    // Stats
    [SerializeField] TowerInfoSO towerInfo;
    public float Damage { get; private set; }  = 10f;
    public float AttackDist { get; private set; } = 10f;
    public float AttackCD { get; private set; } = 1f;

    //Targeting
    public List<GameObject> targets = new List<GameObject>();

    // Projectile
    [SerializeField] private ProjectileLogic projectile;
    [SerializeField] private GameObject projSpawn;

    // Calculations/Timesrs
    private float attackTime = 0.0f;

    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();

        LoadInfo();

        Debug.Assert(projectile != null, $"{gameObject.name} : Missing projectile prefab!");

        if(GameManager.Instance.GetNetworkSettings().isServer == false)
        {
            Destroy(GetComponent<TowerFSM>());
            Destroy(GetComponent<NavMeshObstacle>());
            return;
        }

        photonView.RPC("SetStartingHealthTower", RpcTarget.All, MaxHealth);
    }

    private void LoadInfo()
    {
        if (towerInfo != null)
        {
            Health = towerInfo.BaseHealth;
            MaxHealth = Health;
            Damage = towerInfo.BaseDamage;
            AttackDist = towerInfo.AttackDist;
            AttackCD = towerInfo.AttackCD;
        }
        else
        {
            Debug.LogWarning($"{gameObject.name} : Missing TowerInfo Scriptable Object");
        }
    }

    public void Attack(GameObject _target)
    {
        if (projectile == null)
            return;

        Vector3 projSpawnPos = projSpawn != null ? projSpawn.transform.position : transform.position + new Vector3(0f, 2f);

        GameObject projectileObject = PhotonNetwork.Instantiate(projectile.name, projSpawnPos, Quaternion.identity);

        ProjectileLogic projectileLogic = projectileObject.GetComponent<ProjectileLogic>();

        projectileLogic.SetTarget(_target);
        projectileLogic.SetDamage(Damage);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttackDist);
    }

    public override void TakeDamage(float _damage)
    {
        photonView.RPC("TakeDamageTowerRPC", RpcTarget.All, _damage);
    }

    // rpc function to sync health of the tower
    [PunRPC]
    private void TakeDamageTowerRPC(float _damage)
    {
        Health -= _damage;

        if (onHealthChanged != null)
            onHealthChanged(Health, MaxHealth);

        if (Health <= 0)
        {
            Die();
        }
    }

    [PunRPC]
    private void SetStartingHealthTower(float _health)
    {
        Health = _health;
    }

}
