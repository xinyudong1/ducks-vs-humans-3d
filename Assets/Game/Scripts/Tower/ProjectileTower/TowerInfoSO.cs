using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BasicTowerStats", menuName = "ScriptableObjects/Tower Stats/Basic Tower Stats", order = 1)]
public class TowerInfoSO : ScriptableObject
{
    public float BaseHealth;
    public float BaseDamage;
    public float AttackDist;
    public float AttackCD;
}
