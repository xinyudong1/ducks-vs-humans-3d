using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLogic : MonoBehaviourPun
{
    [SerializeField] private float damage = 10f;
    [SerializeField] private bool followTarget = false;
    [SerializeField] private float checkCD = 0.25f;
    private float checkTimer = 0.0f;
    [SerializeField] private GameObject target;
    public Effect effect;
    public enum Effect
    {
        Heal, Damage, DamageTower
    }

    private void Start()
    {
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
        {
            Destroy(GetComponent<Collider>());
            Destroy(GetComponent<ConstantForce>());
            Destroy(GetComponent<Rigidbody>());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Have to refactor if we want to make a projectile shooting duck attack towers
        DuckLogic duck = other.GetComponent<DuckLogic>();

        if (duck != null)
        {
            switch (effect)
            {
                case Effect.Heal:
                    {
                        if (duck == target.GetComponent<DuckLogic>())
                        {
                            Debug.Log($"Applied heal to {duck.name}, health went from {duck.GetHealth()} to {duck.GetHealth() + damage}");
                            duck.ApplyHeal(damage);
                            PhotonNetwork.Destroy(gameObject);
                        }
                        break;
                    }
                case Effect.Damage:
                    {
                        duck.TakeDamage(damage);
                        PhotonNetwork.Destroy(gameObject);
                        break;
                    }
            }

        }

        Tower tower = other.GetComponent<Tower>();
        if (tower != null)
        {
            switch (effect)
            {
                case Effect.DamageTower:
                    {
                        tower.TakeDamage(damage);
                        PhotonNetwork.Destroy(gameObject);
                        break;
                    }
            }
        }
        BaseController baseobj = other.GetComponent<BaseController>();
        if (baseobj != null)
        {
            switch (effect)
            {
                case Effect.DamageTower:
                    {
                        baseobj.TakeDamage(damage);
                        PhotonNetwork.Destroy(gameObject);
                        break;
                    }
            }
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;

        if (followTarget)
        {
            if (target != null && target.activeSelf)
            {
                if (checkTimer > 0.0f)
                {
                    checkTimer -= Time.deltaTime;
                }
                else
                {
                    checkTimer = checkCD;
                    transform.LookAt(target.transform);
                }
            }
            else
            {
                PhotonNetwork.Destroy(gameObject);
            }
        }
    }

    public void SetTarget(GameObject targetGO)
    {
        target = targetGO;
        transform.LookAt(target.transform);
    }

    public void SetDamage(float _damage)
    {
        damage = _damage;
    }
}
