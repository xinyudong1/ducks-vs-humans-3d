using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class TowerPlacer : MonoBehaviour
{
    [Serializable]
    public enum TowerType
    {
        Projectile,
        Mining,
        AOE
    }
    
    [Serializable]
    public struct TowerInfo
    {
        public TowerType type;
        public uint cost;
    }

    [SerializeField] Material cantPlaceMaterial;
    [SerializeField] Material canPlaceMaterial;
   
    public Transform followTransform;

    public Vector3 transformOffset;

    private bool canPlace = true;
    private MeshRenderer meshRenderer;
    [SerializeField] private PlayerLogic playerCache;

    [SerializeField] List<TowerInfo> towerInfoList = new List<TowerInfo>();
    Dictionary<TowerType, TowerInfo> towerInfoDict = new Dictionary<TowerType, TowerInfo>();
    public Action TowerPlacedListener;

    public bool CanPlace 
    {
        get
        {
            return canPlace;
        }
        private set
        {
            canPlace = value;

            if(canPlace && canPlaceMaterial != null)
            {
                meshRenderer.material = canPlaceMaterial;
            }
            else if (cantPlaceMaterial != null)
            {
                meshRenderer.material = cantPlaceMaterial;
            }
        }
    }

    public TowerInfo GetTowerInfo(TowerType type)
    {
        if(towerInfoDict.TryGetValue(type, out TowerInfo towerInfo))
            return towerInfoDict[type];
        return new TowerInfo { type = TowerType.Projectile, cost = 0};
    }

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();

        if(towerInfoList.Count > 0)
        {
            foreach (TowerInfo info in towerInfoList)
            {
                if(!towerInfoDict.ContainsKey(info.type))
                    towerInfoDict.Add(info.type, info);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate() // Don't do that every frame
    {
        transform.position = followTransform.position + transformOffset;
        
        if (Physics.Raycast(transform.position - transformOffset, Vector3.down, out RaycastHit hit, transformOffset.y))
        {
            transform.position = hit.point + transformOffset;
            Debug.DrawRay(transform.position - transformOffset, Vector3.down * hit.distance, Color.yellow);
        }
        else
        {
            Debug.DrawRay(transform.position - transformOffset, Vector3.down * transformOffset.y, Color.white);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        CanPlace = false;
    }
    private void OnTriggerExit(Collider other)
    {
        CanPlace = true;
    }

    public void PlaceTower(TowerType towerType)
    {
        if (!canPlace)
            return;

        if(towerInfoDict.ContainsKey(towerType))
        {
            TowerInfo info = towerInfoDict[towerType];

            if (info.cost <= playerCache.GetGold())
            {
                GameManager.Instance.RemoveGold(info.cost);

                //Instantiate(info.towerPrefab, transform.position, Quaternion.identity);
                TowerSpawnManager.Instance.SpawnTower_RPC(info.type, transform.position);

                TowerPlacedListener.Invoke();
            }
        }
    }

    public void SetPlayer(PlayerLogic player)
    {
        playerCache = player;
    }
}
