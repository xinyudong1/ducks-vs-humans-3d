using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RoomLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField] TextMeshProUGUI loadingText;
    [SerializeField] TextMeshProUGUI waitForPlayersText;
    [SerializeField] TextMeshProUGUI playersText;
    [SerializeField] NetworkSettingsSO networkSettings;
    [SerializeField] private SceneReference gameScene;
    [SerializeField] private GameObject startButton;
    List<Player> players;


    public override void OnEnable()
    {
        base.OnEnable();
        startButton.SetActive(false);
        players = new List<Player>(PhotonNetwork.PlayerList);
        if (players.Count > 3)
        {
            PhotonNetwork.LeaveRoom();
        }
        
        SetServer();
        DisplayPlayers();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        players.Add(newPlayer);

        if(networkSettings.isServer == true && players.Count == 3)
            startButton.SetActive(true);
        
        DisplayPlayers();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        players.Remove(otherPlayer);
        
        if(PhotonNetwork.CountOfPlayers < 3)
        {
            startButton.SetActive(false);
        }
        
        DisplayPlayers();
    }

    private void DisplayPlayers()
    {
        playersText.text = "";
        foreach (Player player in players)
        {
            Debug.Log(player.NickName);
            string[] playerName = player.NickName.Split(':');
            string name = "";
            for (int i = 1; i < playerName.Length; i++)
            {
                name += playerName[i];
            }
            playersText.text = playersText.text + name + '\n';
        }
    }

    private void SetServer()
    {
        networkSettings.isServer = true;
        
        if (players == null || players.Count == 0)
            return;
        
        // display all the players in the room
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            Debug.Log(player.NickName);

            // If we are the second 3D player, we are not the host
            if (player.NickName.Split(':')[0] == "3D Player" && player.IsLocal == false)
            {
                networkSettings.isServer = false;
            }
        }


        if (PhotonNetwork.CurrentRoom.PlayerCount < 3)
        {
            Debug.Log("Waiting for until three players");
            waitForPlayersText.text = "Waiting for until three players";
        }
        else
        {
            if(networkSettings.isServer == true)
                startButton.SetActive(true);
        }

    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();

    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        MenuManager.Instance.HideMenu(MenuManager.Instance.RoomLobbyMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.GameLobbyMenu);
    }

    public void StartGame()
    {
        photonView.RPC("StartGameRPC", RpcTarget.All);
    }

    [PunRPC]
    private void StartGameRPC()
    {
        MenuManager.Instance.HideMenu(MenuManager.Instance.RoomLobbyMenu);
        CameraManager.Instance.DisableCamera(CameraManager.Instance.UISceneCamera);
        PhotonNetwork.LoadLevel(gameScene, LoadSceneMode.Additive);
        
    }
}
