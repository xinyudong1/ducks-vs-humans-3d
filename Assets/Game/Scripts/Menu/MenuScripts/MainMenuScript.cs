using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] PlayerInfo playerInfo;
    [SerializeField] TMP_InputField nickNameField;
    public void PlayButton_Click()
    {
        playerInfo.NickName = nickNameField.text;
        MenuManager.Instance.HideMenu(MenuManager.Instance.MainMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.LoadNetworkMenu);
    }

    public void HowToPlayButton_Click()
    {
        MenuManager.Instance.HideMenu(MenuManager.Instance.MainMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.InfoMenu);
    }
}