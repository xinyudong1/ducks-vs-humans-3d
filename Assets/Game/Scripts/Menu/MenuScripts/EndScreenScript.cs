using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScreenScript : MonoBehaviourPunCallbacks
{
    public void ReturnToLobby()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();

        MenuManager.Instance.HideMenu(MenuManager.Instance.WinScreen);
        MenuManager.Instance.HideMenu(MenuManager.Instance.LoseScreen);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.GameLobbyMenu);
    }
}
