using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HUDScript : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private float gameTime = 60f * 10f;
    private float currentTime;
    private PhotonView photonView;
   
    private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }
    private void OnEnable()
    {
        currentTime = gameTime;

    }
    private void Update()
    {
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;

        currentTime -= Time.deltaTime;
        photonView.RPC("SetTimer", RpcTarget.All, (int)currentTime);
        if (currentTime <= 0)
        {
            photonView.RPC("DefendersWin", RpcTarget.All);
        }
    }

    [PunRPC]
    private void SetTimer(int time)
    {
        // get the time from time seconds to a time span minutes:seconds
        TimeSpan timeSpan = TimeSpan.FromSeconds(time);
        timerText.text = timeSpan.ToString("mm':'ss");

        if (GameManager.Instance.GetNetworkSettings().isServer == false)
        {
            return;
        }

        if (time <= 0)
        {
            GameManager.Instance.NetworkManager3D.Win();   
        }
    }

}
