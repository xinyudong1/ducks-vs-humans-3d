using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager2D : MonoBehaviourPun
{
    [SerializeField] private NetworkSettingsSO networkSettings;


    void Start()
    {
        GameManager.Instance.NetworkManager2D = this;

        photonView.RPC("ChatMessage", RpcTarget.All, "Hello from ", PhotonNetwork.NickName);
    }


    [PunRPC]
    void ChatMessage(string a, string b)
    {
        Debug.Log(string.Format("ChatMessage {0} {1}", a, b));
    }

    // SpawnUnit RPC that will send a message with a unit GameObject and position
    [PunRPC]
    void SpawnDuckRPC(DUCKTYPE type, Vector3 position)
    {
        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;

        DuckSpawnManager.Instance.SpawnDuck(type, position);
        Debug.Log("Spawn Duck from the 2D side");
    }


}
