using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkMenuManager : MonoBehaviour
{
    public void LoadLoadingScene()
    {
        SceneManager.LoadScene("Loading");
    }

    public void LoadWinnerScene()
    {
        SceneManager.LoadScene("Winner");
    }
}
