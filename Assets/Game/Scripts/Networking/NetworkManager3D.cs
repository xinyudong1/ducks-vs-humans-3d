using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager3D : MonoBehaviourPun
{
    [SerializeField] private NetworkSettingsSO networkSettings;
    [SerializeField] private BaseInfoSO baseInfo;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.NetworkManager3D = this;

        if (GameManager.Instance.GetNetworkSettings().isServer == false)
            return;
        
        // spawn several towers
        PhotonNetwork.Instantiate("ProjectileTower", new Vector3(8, 1, 0), Quaternion.identity);
        PhotonNetwork.Instantiate("ProjectileTower", new Vector3(-3, 1, 15), Quaternion.identity);
        PhotonNetwork.Instantiate("Floor Is Lava Tower", new Vector3(-18, 1, 4), Quaternion.identity);
        PhotonNetwork.Instantiate("ProjectileTower", new Vector3(3, 1, -15), Quaternion.identity);
        PhotonNetwork.Instantiate("Floor Is Lava Tower", new Vector3(18, 1, -4), Quaternion.identity);

        PhotonNetwork.Instantiate(baseInfo.prefab.name, baseInfo.spawnPosition.position, Quaternion.identity);
    }

    public void Lose()
    {
        photonView.RPC("DucksWin", RpcTarget.All);
    }

    public void Win()
    {
        photonView.RPC("HumansWin", RpcTarget.All);
    }

    [PunRPC]
    private void HumansWin()
    {
        SceneLoader.Instance.UnloadScene(SceneLoader.Instance.GameScene);
        CameraManager.Instance.EnableCamera(CameraManager.Instance.UISceneCamera);
        
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        
        MenuManager.Instance.HideMenu(MenuManager.Instance.HudMenu);
        MenuManager.Instance.HideMenu(MenuManager.Instance.PauseMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.WinScreen);
    }

    [PunRPC]
    private void DucksWin()
    {
        SceneLoader.Instance.UnloadScene(SceneLoader.Instance.GameScene);
        CameraManager.Instance.EnableCamera(CameraManager.Instance.UISceneCamera);
        
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        
        MenuManager.Instance.HideMenu(MenuManager.Instance.HudMenu);
        MenuManager.Instance.HideMenu(MenuManager.Instance.PauseMenu);
        MenuManager.Instance.ShowMenu(MenuManager.Instance.LoseScreen);

    }
}
