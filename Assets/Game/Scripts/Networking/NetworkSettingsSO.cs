using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NetworkSettings", menuName = "ScriptableObjects/NetworkSettings")]
public class NetworkSettingsSO : ScriptableObject
{
    public bool isServer = true;
    public string roomName = "";
    public bool createRoom = false;
}
