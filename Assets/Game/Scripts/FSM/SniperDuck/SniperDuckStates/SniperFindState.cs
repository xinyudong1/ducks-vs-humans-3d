using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperFindState : SniperDuckBaseState
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.isStopped = false;
        towers = GameManager.Instance.GetTowers();

        if (towers.Count == 0)
        {
            duck.SetTarget(GameManager.Instance.defenderBase.gameObject);
            Debug.Log("No towers left, going for base");
        }
        else if(duck.targetTower==null)
        {
            Vector3 currentPos = duck.transform.position;
            float dist = float.MaxValue;
            foreach (Tower tower in towers)
            {
                float newDist = Vector3.Distance(currentPos, tower.transform.position);
                if (newDist < dist)
                {
                    dist = newDist;
                    duck.SetTarget(tower.gameObject);
                }
            }
            Debug.Log($"Found a tower to smash. {duck.targetTower.name}");
        }

        //Do math for distance to target and destination here
        Vector3 diff = (duck.transform.position - duck.targetTower.transform.position).normalized;
        diff *= duck.GetBaseStats().attack_dist - 1;
        Vector3 targetPos = duck.targetTower.transform.position + diff;
        agent.SetDestination(targetPos);


        fsm.ChangeState(fsm.navigate);
    }
}
