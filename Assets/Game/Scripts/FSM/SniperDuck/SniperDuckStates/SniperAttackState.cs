using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperAttackState : SniperDuckBaseState
{
    private float attackCD;
    private float attackIntervalCheck = 0.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged += NewTargetCallback;

        attackCD = duck.GetBaseStats().attack_cd;
        if(duck.targetTower==null)
        {
            fsm.ChangeState(fsm.targetFind);
        }

        Ray ray = new Ray(new Vector3(duck.transform.position.x, duck.transform.position.y * 0.5f, duck.transform.position.z), new Vector3(duck.targetTower.transform.position.x, duck.targetTower.transform.position.y * 0.5f, duck.targetTower.transform.position.z));
        if (Physics.Raycast(ray, out RaycastHit hit, 100.0f))
        {
            if (hit.collider.gameObject != duck.targetTower)
            {
                Debug.Log("Raycast hit in SniperAttackState = " + hit.collider.gameObject.name);
                fsm.ChangeState(fsm.specialPathing);
            }
        }

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower != null)
        {
            if (attackIntervalCheck >= attackCD)
            {
                if (duck.projectile == null)
                    return;

                Vector3 projSpawnPos = duck.projSpawnPosition != null ? duck.projSpawnPosition.transform.position : duck.transform.position + new Vector3(0f, 2f);
                PhotonNetwork.Instantiate(duck.projectile.name, projSpawnPos, new Quaternion(0.0f,0.0f,90.0f,0.0f));

                duck.projectile.SetTarget(duck.targetTower);
                duck.projectile.SetDamage(duck.GetBaseStats().attack);
                duck.animator.SetTrigger("Attack");
                attackIntervalCheck = 0.0f;

                agent.transform.LookAt(duck.targetTower.transform);
            }
            else
            {
                attackIntervalCheck += Time.deltaTime;
            }
        }
        else
        {
            fsm.ChangeState(fsm.targetFind);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged -= NewTargetCallback;
    }

    private void NewTargetCallback(GameObject obj)
    {
        fsm.ChangeState(fsm.targetFind);
        duck.onTargetChanged -= NewTargetCallback;
    }
}
