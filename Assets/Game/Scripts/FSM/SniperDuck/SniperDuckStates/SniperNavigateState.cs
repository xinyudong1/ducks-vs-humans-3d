using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperNavigateState : SniperDuckBaseState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged += NewTargetCallback;

        if (duck.targetTower == null)
        {
            fsm.ChangeState(fsm.targetFind);
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower != null)
        {
            if (agent.remainingDistance <= 0.1f)
            {
                agent.isStopped = true;
                fsm.ChangeState(fsm.attackState);
            }
        }
        else if (duck.targetTower == null)
        {
            fsm.ChangeState(fsm.targetFind);
        }

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged -= NewTargetCallback;
    }

    private void NewTargetCallback(GameObject obj)
    {
        fsm.ChangeState(fsm.targetFind);
        duck.onTargetChanged -= NewTargetCallback;
    }
}