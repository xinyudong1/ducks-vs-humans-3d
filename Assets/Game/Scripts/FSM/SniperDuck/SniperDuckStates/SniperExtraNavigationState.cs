using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperExtraNavigationState : SniperDuckBaseState
{
    private float timerCheck = 1.0f;
    private float elapsedTime = 0.0f;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.isStopped = false;
        if (duck.targetTower == null)
        { fsm.ChangeState(fsm.targetFind); }

        duck.onTargetChanged += NewTargetCallback;
    }



    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower == null)
        {
            fsm.ChangeState(fsm.targetFind);
        }
        if (elapsedTime >= timerCheck)
        {
            Ray ray = new Ray(new Vector3(duck.transform.position.x, duck.transform.position.y * 0.5f, duck.transform.position.z), new Vector3(duck.targetTower.transform.position.x, duck.targetTower.transform.position.y * 0.5f, duck.targetTower.transform.position.z));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.collider.gameObject == duck.targetTower)
                {
                    Debug.Log("Special Nav State Raycast Hit = " + hit.rigidbody.gameObject.name);
                    agent.isStopped = true;
                    fsm.ChangeState(fsm.attackState);
                }
                else
                {
                    agent.SetDestination(duck.targetTower.transform.position);
                }
            }
            elapsedTime = 0.0f;
        }
        else
        {
            elapsedTime += Time.deltaTime;
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged -= NewTargetCallback;
    }

    private void NewTargetCallback(GameObject obj)
    {
        fsm.ChangeState(fsm.targetFind);
        duck.onTargetChanged -= NewTargetCallback;
    }
}
