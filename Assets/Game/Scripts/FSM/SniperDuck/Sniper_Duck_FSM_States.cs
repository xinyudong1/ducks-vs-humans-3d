using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(DuckLogic))]
public class Sniper_Duck_FSM_States : FSM
{
    //Add all the FSM States stringtohash for the states you want to move to/from
    public readonly int navigate = Animator.StringToHash("Navigate");
    public readonly int targetFind = Animator.StringToHash("TargetFind");
    public readonly int attackState = Animator.StringToHash("Attack");
    public readonly int specialPathing = Animator.StringToHash("SpecialPathing");
}
