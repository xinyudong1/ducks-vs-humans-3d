using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SniperDuckBaseState : FSMBaseState<Sniper_Duck_FSM_States>
{
    protected DuckLogic duck;
    protected NavMeshAgent agent;
    protected List<Tower> towers;
    public override void Init(GameObject _owner, FSM _fsm)
    {
        base.Init(_owner, _fsm);

        duck = owner.GetComponent<DuckLogic>();
        Debug.Assert(duck != null, $"{owner.name} requires a DuckLogic Component");

        agent = owner.GetComponent<NavMeshAgent>();
        Debug.Assert(agent != null, $"{owner.name} requires a NavMeshAgent Component");
    }
}
