using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorisLavaTowerFSM : FSM
{
    //Put your FSM variables here to be able to change states
    public readonly int IdleState = Animator.StringToHash("IdleState");
    public readonly int AttackState = Animator.StringToHash("AttackState");
}