using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorisLavaTowerBaseState : FSMBaseState<FloorisLavaTowerFSM>
{
    protected LavaTower tower;

    public override void Init(GameObject _owner, FSM _fsm)
    {
        base.Init(_owner, _fsm);

        tower = owner.GetComponent<LavaTower>();
        Debug.Assert(tower != null, $"{owner.name} is missing LavaTower component");
    }
}

