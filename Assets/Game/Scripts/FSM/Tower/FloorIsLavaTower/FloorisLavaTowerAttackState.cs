using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class FloorisLavaTowerAttackState : FloorisLavaTowerBaseState
{
    private float attackTimer = 0.0f;
    private float attackCD = 0.0f;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
    {
        attackCD = tower.AttackCD;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (attackCD <= attackTimer)
        {
            if (tower.targets.Count > 0)
            {
                tower.Attack();
                attackTimer = 0.0f;
            }
            else
            {
                fsm.ChangeState(fsm.IdleState);
            }
        }
        else
        {
            attackTimer += Time.deltaTime;
        }
    }
}
