using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAttackState : TowerBaseState
{
    private float attackTimer = 0.0f;
    private float attackCD = 0.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        attackCD = tower.AttackCD;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (attackTimer > 0.0f)
        {
            attackTimer -= Time.deltaTime;
        }
        else if (tower.targets.Count > 0)
        {
            attackTimer = attackCD;
            tower.Attack(tower.targets[0]);
        }
        else
        {
            fsm.ChangeState(fsm.IdleState);
        }
    }
}
