using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerIdleState : TowerBaseState
{
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(tower.targets.Count > 0)
        {
            fsm.ChangeState(fsm.AttackState);
        }
    }
}
