using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(TowerLogic))]
public class TowerFSM : FSM
{
    //Add all the FSM States stringtohash for the states you want to move to/from
    public readonly int IdleState = Animator.StringToHash("IdleState");
    public readonly int AttackState= Animator.StringToHash("AttackState");
}
