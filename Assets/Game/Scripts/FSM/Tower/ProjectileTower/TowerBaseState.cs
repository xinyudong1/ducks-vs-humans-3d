using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBaseState : FSMBaseState<TowerFSM>
{
    protected TowerLogic tower;
    protected List<DuckLogic> ducks;

    public override void Init(GameObject _owner, FSM _fsm)
    {
        base.Init(_owner, _fsm);

        tower = owner.GetComponent<TowerLogic>();
        Debug.Assert(tower != null, $"{owner.name} requires a Tower Component");
    }
}
