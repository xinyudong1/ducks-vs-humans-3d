using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(DuckLogic))]
public class Medic_Duck_FSM_States : FSM
{
    //Add all the FSM States stringtohash for the states you want to move to/from
    public readonly int navigate = Animator.StringToHash("Navigate");
    public readonly int targetFind = Animator.StringToHash("TargetFind");
    //public readonly int Navigate = Animator.StringToHash("Navigate");
    public readonly int HealState = Animator.StringToHash("HealUnit");


}