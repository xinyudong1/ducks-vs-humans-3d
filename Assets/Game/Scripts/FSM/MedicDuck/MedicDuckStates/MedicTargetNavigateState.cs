using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicTargetNavigateState : MedicDuckBaseState
{

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower == null)
        {
            fsm.ChangeState(fsm.targetFind);
        }
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower != null)
        {
            // Keep walking to the tower, or start attacking
            if (duck.GetBaseStats().attack_dist >= Vector3.Distance(duck.transform.position, agent.destination))
            {
                agent.isStopped = true;
                fsm.ChangeState(fsm.HealState);
            }
        }
        else if(duck.targetTower==null)
        {
            fsm.ChangeState(fsm.targetFind);
        }
    }
}
