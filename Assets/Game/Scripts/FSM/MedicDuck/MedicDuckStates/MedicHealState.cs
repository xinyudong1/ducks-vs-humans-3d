using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicHealState : MedicDuckBaseState
{
    private DuckLogic toHealUnit;
    private float healInterval;
    private float healIntervalCheck = 0.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower != null)
        {
            healInterval = duck.GetBaseStats().attack_cd;
        }
        else
        {
            fsm.ChangeState(fsm.targetFind);
        }

        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower != null)
        {
            if (Vector3.Distance(duck.gameObject.transform.position, duck.targetTower.transform.position) < duck.GetBaseStats().attack_dist)
            {
                if (healIntervalCheck >= healInterval)
                {
                    if (duck.projectile == null)
                        return;

                    //Copied from TowerLogic
                    Vector3 projSpawnPos = duck.projSpawnPosition != null ? duck.projSpawnPosition.transform.position : duck.transform.position + new Vector3(0f, 2f);
                    PhotonNetwork.Instantiate(duck.projectile.name, projSpawnPos, Quaternion.identity);

                    duck.projectile.SetTarget(duck.targetTower);
                    duck.projectile.SetDamage(duck.GetBaseStats().attack);

                    duck.animator.SetTrigger("shoot");
                    healIntervalCheck = 0.0f;

                    Debug.Log("Duck heal stats" + duck.targetTower.GetComponent<DuckLogic>().GetHealth() + " % health to check " + duck.targetTower.GetComponent<DuckLogic>().GetBaseStats().base_health * 0.8f);
                    if (duck.targetTower.GetComponent<DuckLogic>().GetHealth() > (duck.targetTower.GetComponent<DuckLogic>().GetBaseStats().base_health * 0.8f))
                    {
                        findNewDuckToHeal = true;
                        fsm.ChangeState(fsm.targetFind);
                    }
                }

                else
                {
                    healIntervalCheck += Time.deltaTime;
                }
            }
            else
            {
                fsm.ChangeState(fsm.targetFind);
            }
        }
        else
        {
            fsm.ChangeState(fsm.targetFind);
        }
    }
}
