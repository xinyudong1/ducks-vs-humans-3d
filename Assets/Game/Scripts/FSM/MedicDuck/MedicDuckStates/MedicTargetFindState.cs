using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class MedicTargetFindState : MedicDuckBaseState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //If the duck wasnt targeting something before, find a target now. Else you just path to its current position in the second if statement
        if (duck.targetTower == null || findNewDuckToHeal == true)
        {
            findNewDuckToHeal = false;
            allDucks = GameManager.Instance.GetDucks();

            foreach (DuckLogic duky in allDucks)
            {
                if (duky == duck)
                {
                    allDucks.Remove(duck);
                }
            }

            foreach (DuckLogic duky in allDucks)
            {
                if (duck.targetTower == null && allDucks.Count != 0)
                {
                    duck.SetTarget(allDucks[0].gameObject);
                }
                
                if(duky.GetHealth() < duck.targetTower.GetComponent<DuckLogic>().GetHealth())
                {
                    
                    duck.SetTarget(duky.gameObject);
                }

            }

        }
        if (duck.targetTower != null)
        {
            agent.isStopped = false;
            Vector3 diff = (duck.transform.position - duck.targetTower.transform.position).normalized;
            diff *= duck.GetBaseStats().attack_dist - 3;
            Vector3 targetPos = duck.targetTower.transform.position + diff;
            agent.SetDestination(targetPos);
        }

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower == null)
        {
            fsm.ChangeState(fsm.navigate);
        }
        fsm.ChangeState(fsm.navigate);
    }
}
