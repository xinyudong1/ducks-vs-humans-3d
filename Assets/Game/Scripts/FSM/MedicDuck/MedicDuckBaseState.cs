using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicDuckBaseState : FSMBaseState<Medic_Duck_FSM_States>
{
    protected DuckLogic duck;
    protected UnityEngine.AI.NavMeshAgent agent;
    protected List<DuckLogic> allDucks;
    protected bool findNewDuckToHeal = false;

    //Dont really need the animator since the only animation going to be played is the attack and that has been cached under the duck directly. 
    //protected Animator animator;

    public override void Init(GameObject _owner, FSM _fsm)
    {
        base.Init(_owner, _fsm);

        duck = owner.GetComponent<DuckLogic>();
        Debug.Assert(duck != null, $"{owner.name} requires a DuckLogic Component");

        agent = owner.GetComponent<UnityEngine.AI.NavMeshAgent>();
        Debug.Assert(agent != null, $"{owner.name} requires a NavMeshAgent Component");

        //animator= owner.GetComponent<Animator>();
        //Debug.Assert(animator != null, $"{owner.name} requires an Animator");
    }

}
