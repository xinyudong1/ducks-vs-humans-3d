using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoutTargetAttackState : ScoutDuckBaseState
{
    private float attackInterval;
    private float attackIntervalCheck = 0.0f;

    private bool isTower = false;
    private bool isBase = false;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        isBase = false;
        isTower = false;
        duck.onTargetChanged += NewTargetCallback;
        if (duck.targetTower == null)
        {
            fsm.ChangeState(fsm.FindBase);
        }
        else
        {
            attackInterval = duck.GetBaseStats().attack_cd;
        }

        if (duck.targetTower.GetComponent<Tower>() != null)
        {
            isTower = true;
        }
        else if (duck.targetTower.GetComponent<BaseController>() != null)
        {
            isBase = true;
        }

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower != null)
        {
            if (Vector3.Distance(duck.gameObject.transform.position, duck.targetTower.transform.position) < duck.GetBaseStats().attack_dist)
            {
                if (attackIntervalCheck >= attackInterval)
                {
                    duck.animator.SetTrigger("attack");
                    if (isTower)
                    {
                        duck.targetTower.GetComponent<Tower>().TakeDamage(duck.GetBaseStats().attack);
                    }
                    else if (isBase)
                    {
                        duck.targetTower.GetComponent<BaseController>().TakeDamage(duck.GetBaseStats().attack);
                    }
                    attackIntervalCheck = 0.0f;
                }
                else
                {
                    attackIntervalCheck += Time.deltaTime;
                }
            }
            else
            {
                fsm.ChangeState(fsm.FindBase);
            }
        }
        else
            fsm.ChangeState(fsm.FindBase);

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged -= NewTargetCallback;
    }

    private void NewTargetCallback(GameObject obj)
    {
        fsm.ChangeState(fsm.FindBase);
        duck.onTargetChanged -= NewTargetCallback;
    }
}
