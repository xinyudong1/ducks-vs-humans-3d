using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoutTargetFindState : ScoutDuckBaseState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower == null)
        {
            duck.SetTarget(GameManager.Instance.defenderBase.gameObject);
        }
        
        Vector3 diff = (duck.transform.position - duck.targetTower.transform.position).normalized;
        diff *= duck.GetBaseStats().attack_dist - 1;
        Vector3 targetPos = duck.targetTower.transform.position + diff;
        agent.SetDestination(targetPos);
        fsm.ChangeState(fsm.Navigate);
    }

}
