using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent), typeof(DuckLogic))]
public class Scout_Duck_FSM_States : FSM
{
    //Add all the FSM States stringtohash for the states you want to move to/from
    public readonly int AttackState = Animator.StringToHash("Attack");
    public readonly int FindBase = Animator.StringToHash("FindBase");
    public readonly int Navigate = Animator.StringToHash("Navigate");
}
