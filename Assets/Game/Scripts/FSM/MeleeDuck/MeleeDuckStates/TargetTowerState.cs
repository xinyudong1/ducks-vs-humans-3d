using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTowerState : MeleeDuckBaseState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        towers = GameManager.Instance.GetTowers();

        //If there are no towers to destroy, it goes for their base.
        if (towers.Count == 0)
        {
            duck.SetTarget(GameManager.Instance.defenderBase.gameObject);
            Debug.Log($"No ducks left, going for the base.{duck.targetTower.name}");
            agent.SetDestination(duck.targetTower.transform.position);
        }
        else
        {
            if (duck.targetTower == null)
            {
                Vector3 duckPosition = duck.transform.position;
                float currentDist = float.MaxValue;
                foreach (Tower tower in towers)
                {
                    if (Vector3.Distance(duckPosition, tower.transform.position) < currentDist)
                    {
                        duck.SetTarget(tower.gameObject);
                        currentDist = Vector3.Distance(duckPosition, tower.transform.position);
                    }
                }
                Debug.Log($"Found Target to kill.{duck.targetTower.name}");
                //agent.SetDestination(duck.targetTower.transform.position);
            }
            if (duck.targetTower != null)
            {
                Vector3 diff = (duck.transform.position - duck.targetTower.transform.position).normalized;
                diff *= duck.GetBaseStats().attack_dist - 1;
                Vector3 targetPos = duck.targetTower.transform.position + diff;
                agent.SetDestination(targetPos);
            }

        }
        fsm.ChangeState(fsm.Navigate);
    }
}
