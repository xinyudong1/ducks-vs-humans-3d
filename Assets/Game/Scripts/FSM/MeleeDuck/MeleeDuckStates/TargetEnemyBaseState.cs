using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetEnemyBaseState : MeleeDuckBaseState
{
    private float _attackInterval;
    private float attackIntervalCheck = 0.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged += NewTargetCallback;
        _attackInterval = duck.GetBaseStats().attack_cd;

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower == null)
        {
            fsm.ChangeState(fsm.FindTower);
        }
        else
        {
            if (attackIntervalCheck >= _attackInterval)
            {
                duck.targetTower.GetComponent<BaseController>().TakeDamage(duck.GetBaseStats().attack);
                Debug.Log($"Base {duck.targetTower.name} is being attacked");
                attackIntervalCheck = 0.0f;
                duck.animator.SetTrigger("attack");
            }
            else
            {
                attackIntervalCheck += Time.deltaTime;
            }
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged -= NewTargetCallback;
    }

    private void NewTargetCallback(GameObject obj)
    {
        fsm.ChangeState(fsm.FindTower);
        duck.onTargetChanged -= NewTargetCallback;
    }

}