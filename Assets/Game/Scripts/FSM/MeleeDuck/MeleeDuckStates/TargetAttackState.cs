using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetAttackState : MeleeDuckBaseState
{
    // This makes me sad to cache it again and again but it must be done for now.
    private Animator attackAnimator;
    private float _attackInterval;
    private float attackIntervalCheck = 0.0f;
    private bool isTower = false;
    private bool isBase = false;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged += NewTargetCallback;

        _attackInterval = duck.GetBaseStats().attack_cd;
        attackAnimator = duck.animator;

        if (duck.targetTower.GetComponent<Tower>() == true)
        {
            isTower = true;
            isBase = false;
        }
        else if (duck.targetTower.GetComponent<BaseController>() == true)
        {
            isBase = true;
            isTower = false;
        }

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (duck.targetTower == null)
        {
            fsm.ChangeState(fsm.FindTower);
        }
        else
        {
            if (duck.GetBaseStats().attack_dist >= Vector3.Distance(duck.transform.position, agent.destination))
            {
                if (attackIntervalCheck >= _attackInterval)
                {
                    if (isTower)
                    {
                        duck.targetTower.GetComponent<Tower>().TakeDamage(duck.GetBaseStats().attack);
                        Debug.Log($"Tower {duck.targetTower.name} begin attacked");
                        attackIntervalCheck = 0.0f;
                        attackAnimator.SetTrigger("attack");
                    }
                    else if (isBase)
                    {
                        duck.targetTower.GetComponent<BaseController>().TakeDamage(duck.GetBaseStats().attack);
                        Debug.Log($"Base {duck.targetTower.name} is being attacked");
                        attackIntervalCheck = 0.0f;
                        attackAnimator.SetTrigger("attack");
                    }

                }
                else
                {
                    attackIntervalCheck += Time.deltaTime;
                }
            }
            else
            {
                fsm.ChangeState(fsm.Navigate);
            }
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged -= NewTargetCallback;
    }

    private void NewTargetCallback(GameObject obj)
    {
        fsm.ChangeState(fsm.FindTower);
        duck.onTargetChanged -= NewTargetCallback;
    }


}
