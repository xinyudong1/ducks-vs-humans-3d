using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetNavigateState : MeleeDuckBaseState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged += NewTargetCallback;

        if (duck.targetTower == null)
        {
            fsm.ChangeState(fsm.FindTower);
        }
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (duck.targetTower != null)
        {
            // Keep walking to the tower, or start attacking
            if (duck.GetBaseStats().attack_dist >= Vector3.Distance(duck.transform.position, agent.destination))
            {
                agent.isStopped = true;
                fsm.ChangeState(fsm.AttackState);
            }
            else
            {
                agent.isStopped = false;
            }
        }
        else
        {
            fsm.ChangeState(fsm.FindTower);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        duck.onTargetChanged -= NewTargetCallback;
    }

    private void NewTargetCallback(GameObject obj)
    {
        fsm.ChangeState(fsm.FindTower);
        duck.onTargetChanged -= NewTargetCallback;
    }
}
