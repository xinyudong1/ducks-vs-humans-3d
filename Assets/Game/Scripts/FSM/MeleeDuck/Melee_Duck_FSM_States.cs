using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(DuckLogic))]
public class Melee_Duck_FSM_States : FSM
{
    //Add all the FSM States stringtohash for the states you want to move to/from
    public readonly int AttackState = Animator.StringToHash("Attack");
    public readonly int FindTower = Animator.StringToHash("Target Tower");
    public readonly int Navigate = Animator.StringToHash("Navigate");
    public readonly int AttackBase = Animator.StringToHash("AttackBase");

}
